import { Component } from "@angular/core";
import { NavController, Platform } from "ionic-angular";
import { NativeStorage } from "@ionic-native/native-storage";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  contacts = [];

  formData = {
    name: "",
    email: "",
    phone: ""
  };

  showContacts = true;

  constructor(
    public navCtrl: NavController,
    private platform: Platform,
    private nativeStorage: NativeStorage
  ) {}

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.getData();
    });
  }

  clearForm() {
    this.formData = {
      name: "",
      email: "",
      phone: ""
    };
  }

  getData() {
    this.nativeStorage
      .getItem("contacts")
      .then(res => {
        this.contacts = res;
      })
      .catch(() => {
        this.nativeStorage.setItem("contacts", this.contacts);
      });
  }

  addContact() {
    this.nativeStorage
      .setItem("contacts", [...this.contacts, this.formData])
      .then(() => {
        this.clearForm();
        this.getData();
      });
  }

  removeAllContact() {
    this.contacts = [];
    this.nativeStorage.setItem("contacts", []).then(() => {
      this.getData();
    });
  }

  removeContact(contact) {
    let i = this.contacts.indexOf(contact);
    this.nativeStorage
      .setItem("contacts", [
        ...this.contacts.slice(0, i),
        ...this.contacts.slice(i + 1)
      ])
      .then(() => {
        this.getData();
      });
  }

  toggleShowContacts() {
    this.showContacts = !this.showContacts;
  }
}
